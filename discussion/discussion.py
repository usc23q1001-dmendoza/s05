# [SECTION] Python Class Review

class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f'The year is: {self.year}')

myObj = SampleClass(2020)
myObj.show_year()

# [SECTION] Fundamentals of OOP
# There are four main fundamental principles in OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction

# [SECTION] Encapsulation
# Encapsulation is a mechanism of wrapping the attributes and codes acting on the methods together as a single unit.
# 'data hiding'

# The prefix underscore is used as a warning for developers that means:
# 'Please be careful about this attribute or method, don't use it ourside the declared Class.'

# Mini Exercise
    # Add another protected attribute called age and create the necessary getter and setter methods
class Person():
	def __init__(self):
		# protected attribute
		self._name = 'John Doe'
		self._age = 25

	# setter method (modifying)
	def set_person(self, name, age):
		self._name = name
		self._age = age

	# getter method (viewing)
	def get_person(self):
		print(f'Name of person: {self._name}')
		print(f'Age of person: {self._age}')

# print(p1.name) - this will return an attribute error
p1 = Person()
p1.get_person()

p1.set_person('Stacy', 30)
p1.get_person()

# [SECTION] Inheritance
# Inheritance - The transfer of the characteristics of a parent class to child classes are derived form it.
# Parent-Child Relationship
# To create an inherited class, in hte className definition with add the parent class as the parameter of the child class.
# Syntax: class ChildClassName(ParentClassName)

class Employee(Person):
	def __init__(self, employee_id):
		# super() can be used to invoke the immediate parent class constructor.
		super().__init__()
		# unique attribute to the Employee Class
		self.employee_id = employee_id

	# Methods of the Employee Class
	def get_employee_id(self):
		print(f'The Employee ID is {self.employee_id}')

	def set_employee_id(self, employee_id):
		self.employee_id = employee_id

	# Details method
	def get_emp_details(self):
		print(f'{self.employee_id} belongs to {self._name}')

emp1 = Employee('Emp-001')
emp1.get_emp_details()
emp1.get_person()

emp1.set_person('Stacy Cruz', 23)
emp1.get_emp_details()

# Mini exercise
    # 1. Create a new class called Student that inherits Person with the additional attributes and methods
    # attributes: Student No, Course, Year Level
    # methods:
    #   Create the necessary getters and setters for each attribute
    #   get_detail: prints the output "<Student name> is currently in year <year level> taking up <Course>"

class Student(Person):
	def __init__(self, student_no, course, yr_lvl):
		super().__init__()
		self.student_no = student_no
		self.course = course
		self.yr_lvl = yr_lvl

	def set_student(self, student_no, course, yr_lvl):
		self.student_no = student_no
		self.course = course
		self.yr_lvl = yr_lvl

	def get_student(self):
		print(f'Student no: {self.student_no}')
		print(f'Student course: {self.course}')
		print(f'Student year: {self.yr_lvl}')

	def get_stud_details(self):
		print(f'{self._name} is currently in year {self.yr_lvl} taking up {self.course}')

stud1 = Student(19102710, 'BSCS', '3')
stud1.get_student()

emp1.set_person('Mark', 23)

stud1.get_stud_details()

# [SECTION] Polymorphism
# Poly - Many
# morph - Form
# The method inherited from the parent class is not always fit for the child class. Re-implementation/Overriding of method can be done in the child class

# There are different methods to use polymorphism in Python.

# Function and Objects
# A function can be created that can take any object, allowing polymorphism.

class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print('Admin User')

class Customer():
	def is_admin(self):
		print(False)

	def	user_type(self):
		print('Customer User')

# Define a test function that will take an object called obj.
def test_function(obj):
	obj.is_admin()
	obj.user_type()

user_admin = Admin()
user_customer = Customer()

# Pass the created instance to test_function
test_function(user_admin)
test_function(user_customer)

# Polymorphism with Class Methods
# Python uses two different class types in the same way.

class TeamLead():
	def occupation(self):
		print('Team Lead')
	
	def has_auth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print('Team Member')
	
	def has_auth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()

# Polymorphism with Inheritance
# Polymorphism in python defines methods in child class that have the same name as methods in the parent class.
# "Method Overriding"

class Zuitt():
	def tracks(self):
		print('We are currently offering 3 tracks(developer career, pi-shape career and short courses)')

	def num_of_hours(self):
		print('Learn web development in 360 hours')

class DeveloperCareer(Zuitt):
	# Override the parent's num_of_hours() method
	def num_of_hours(self):
		print('Learn the basics of web development in 240 hours!')

class PiShapeCareer(Zuitt):
	# Override the parent's num_of_hours() method
	def num_of_hours(self):
		print('Learn the skills for no-code app development in 140 hours!')

class ShortCourses(Zuitt):
	# Override the parent's num_of_hours() method
	def num_of_hours(self):
		print('Learn advance topics in web development in 20 hours!')

course1 = DeveloperCareer()
course2 = PiShapeCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

# [SECTION] Abstraction
# An abstract class can be considered as a blue print for other class.

# abc = Abstract Base Classes
# The import tells the program to get the abc module of python to be used
from abc import ABC, abstractclassmethod

# The class polygon inherits the abstract class module.
class Polygon(ABC):
	# Created an abstract methods called print_num_of_sides that needs to be implemented by classes that will inherit polygon class.
	@abstractclassmethod
	def print_num_of_sides(self):
		# This denotes that the method doesn't do anything.
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	# Since the triangle class inherited the polygon class, it must now implement the abstract method.
	def print_num_of_sides(self):
		print('This Polygon has 3 sides')

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def print_num_of_sides(self):
		print('This Polygon has 5 sides')

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_num_of_sides()
shape2.print_num_of_sides()