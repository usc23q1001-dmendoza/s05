from abc import ABC, abstractclassmethod

class Animal(ABC):
	# Abstraction
	@abstractclassmethod
	def eat(self, food):
		self.food = food

	# Abstraction
	@abstractclassmethod
	def make_sound():
		pass

# Inheritance
class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		# Encapsulation
		self._name = name
		self._breed = breed
		self._age = age

	def set_dog(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def get_dog(self):
		print(f'Dog name: {self._name}')
		print(f'Dog Breed: {self._breed}')
		print(f'Dog Age: {self._age}')

	# Abstraction
	def eat(self, food):
		print(f'Eaten {food}')
	def make_sound(self):
		print('Bark! Woof! Arf!')

	# Polymorphism
	def call(self):
		print(f'Here {self._name}')

# Inheritance
class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		# Encapsulation
		self._name = name
		self._breed = breed
		self._age = age

	def set_cat(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def get_cat(self):
		print(f'Cat name: {self._name}')
		print(f'Cat Breed: {self._breed}')
		print(f'Cat Age: {self._age}')

	# Abstraction
	def eat(self, food):
		print(f'Serve me {food}')
	def make_sound(self):
		print('Miaow! Nyaw! Nyaaaaa!')

	# Polymorphism
	def call(self):
		print('Puss, Come on!')

dog1 = Dog('Isis', 'Dalmatian', 15)
dog1.eat('Steak')
dog1.make_sound()
dog1.call()

cat1 = Cat('Isis', 'Dalmatian', 15)
cat1.eat('Tuna')
cat1.make_sound()
cat1.call()